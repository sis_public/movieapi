# 1. Base image
FROM python:3.8.6-slim-buster

# 2. Copy requirements
COPY requirements.txt /requirements.txt

# 3. Install requirements
RUN pip install -r /requirements.txt

# 4. Copy project files
COPY . /

